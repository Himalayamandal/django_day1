from django.shortcuts import render, HttpResponse

# Create your views here.
# def helloworld(request):
    # return HttpResponse("<h2> Hello World </h2>")

def home(request):
    text = request.GET.get("take_text")
    try:
        result = int(text)*5
    except:
        result = "please insert a number"
    return render(request, "home.html", context={"result":result})
